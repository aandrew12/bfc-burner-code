/* 
 * File:   RS232.h
 * Author: dyulov
 *
 * Created on April 16, 2013, 11:20 PM
 */

#ifndef RS232_H
#define	RS232_H

#ifdef	__cplusplus
extern "C"{
#endif

/* Public function prototypes */
void RS232_Init(void);
void RS232_Write(void);

#ifdef	__cplusplus
}
#endif

#endif	/* RS232_H */


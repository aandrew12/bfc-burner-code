/*
 * File:   control.c
 * Author: dyulov
 *
 * Created on July 25, 2013, 3:30 PM
 *
 * State Machine implementation
 */
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "peripherals.h"
#include "dac.h"
#include "system.h"
#include "injector.h"
#include "types.h"
#include "atemp.h"
#include "etemp.h"
//#include "press.h"
#include "control.h"

#define NO_ENGINE_INJECTOR_TEST 0 // Must be 0 for normal operation
#if (DAC_TYPE == DAC_MCP4802)
#define CAG_PERCENT_ALCOHOL 127 // 8-bit 127 = 90%, 136 = 93%, 156 = 100%
#elif (DAC_TYPE == DAC_MCP4912)
#define CAG_PERCENT_ALCOHOL 420 // 10-bit 420 = 90%, 511 = 100%
#endif

//CHANGE BY AA 9/8/2017 declaring secondsruntime
extern uint16_t SecondsRunTime;
extern uint16_t CAG_Offset;
extern uint16_t CAG_Offset_Limited;
//int16_t EXP_CALC;
uint16_t TempIn;
uint16_t TargetTemp = 413;  //TargetTemp = ADC Temp according to RTD spreadsheet, i.e. 190 degC = 450;  Formerly 413 @ "99"
int16_t DeltaTemp;  //DeltaTemp needs to be able to be negative
uint16_t ScaleFactor = 3;
extern AnalogSignal_t AirTempADC_Filtered;
extern AnalogSignal_t EngTempADC_Filtered;
extern AnalogSignal_t PressADC_Filtered;
extern float BlockTemp = 20.0;
/* File Scope Variables */
// FSM from dmitry
GenCtrlState_t GenCtrlState = GEN_STATE_INIT;
uint8_t GenCtrlFaultCode = 0;
uint16_t TimerInState = 0;
uint16_t UI_InputADC = 0; // UI analog input - fuel type and drain
bool DrainMode = false;
// Engine speed aka ignition pulse input 1 per revolution
uint16_t IGN_PulseTimeOld = 0;
uint16_t IGN_PulseTimeNew = 0;
uint16_t IGN_PulsePeriod = 0;
uint8_t IGN_PulseTimeoutCntr = 0;
uint16_t TimeStep = 30;
uint16_t TimeWait = 1500; //15 seconds, 100 ticks per second
// External interface
bool InjectorEnabled = false;
bool ChargeEnabled = false;
FuelType_t FuelType = FUEL_TYPE_FLT;
bool CalibrationMode = false;

/* File Scope Prototypes */
static void EnterGenStateInit(void);
static void ExecuteGenStateInit(void);
static void EnterGenStateDrain(void);
static void ExecuteGenStateDrain(void);
static void EnterGenStateWait(void);
static void ExecuteGenStateWait(void);
static void EnterGenStateStart(void);
static void ExecuteGenStateStart(void);
static void EnterGenStateRun(void);
static void ExecuteGenStateRun(void);
static void EnterGenStateOff(void);
static void ExecuteGenStateOff(void);
static void EnterGenStateCal(void);
static void ExecuteGenStateCal(void);
void GenCtrlFSM(void);
void BDCFuelPumpEnable(void);
void LSD_FaultCommand(void);
void BlockTempToTimeInit(void);

void GenControlInit(void){
    AirTempInit();
    EngTempInit();
   // PressInit();
    EnterGenStateInit();
}

void GenControl(void){
    AirTempMonitor();
    EngTempMonitor();
    //PressMonitor();
    GenCtrlFSM();
#if NO_ENGINE_INJECTOR_TEST // Used for injector testing without engine
    IGN_PulsePeriod = IGN_PULSE_PERIOD_MIN + 1;
    IGN_PulseTimeoutCntr = TMR_IGN_PULSE_TIMEOUT;
#endif
    // Ign Pulse timeout counter
    if(IGN_PulseTimeoutCntr){IGN_PulseTimeoutCntr--;}
}

static void EnterGenStateInit(void){
    LSD_FaultCommand(); // Setup fault LSD fault handling
    LSD_DISABLE;
    FUEL_PUMP_DISABLE;
    CAG_DISABLE;
    InjectorEnabled = false;
    ChargeEnabled = false;
    GenCtrlState = GEN_STATE_INIT;
}
static void ExecuteGenStateInit(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    if((!AirTempADC_Filtered.IsValid) || (!EngTempADC_Filtered.IsValid)){
        return;
    }
    // Enter calibration mode if 5V is present on Air Temperature sensor input
    if(AirTempADC_Filtered.Value > 0x3cc){
        FuelType = FUEL_TYPE_JP8; // Calibration is with JP8 only
        EnterGenStateCal();
        return;
    }
    
    FuelType=FUEL_TYPE_JP8; // ME 8/18/2017 hardcoded fuel type and omitted UI board read

    // Get state of the Drain switch
    if(DrainMode){
        EnterGenStateDrain();
    }else{
        BlockTempToTimeInit();
        EnterGenStateWait();
    }
}
static void EnterGenStateDrain(void){
    LSD_ENABLE;
    FUEL_PUMP_ENABLE;
    BDCFuelPumpEnable();
    LED_WAIT_ON;
    GenCtrlState = GEN_STATE_DRAIN;
    TimerInState = TMR_DRAIN_TIMEOUT;
}
static void ExecuteGenStateDrain(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    if(!TimerInState--){
        EnterGenStateOff();
        return;
    }
}
static void EnterGenStateWait(void){
    LSD_ENABLE;
    FUEL_PUMP_ENABLE;
    BDCFuelPumpEnable();
    if(FuelType != FUEL_TYPE_GAS){
        CAG_ENABLE;
    }
    GenCtrlState = GEN_STATE_WAIT;
   // TimerInState = TMR_WAIT_TIMEOUT; // TODO: Should be temp compensated
    TimerInState = TimeWait;
}
static void ExecuteGenStateWait(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    else{ //bypassing blinking period, go directly to schedule
        EnterGenStateStart();
        return;
    }

//    if(!(TimerInState%50))LED_WAIT_BLINK;
//    if((!TimerInState--)||IGN_PulseTimeoutCntr){
//        EnterGenStateStart();
//        return;
//    }
//
}
static void EnterGenStateStart(void){
    ChargeEnabled = true;
    LED_WAIT_OFF;
    LED_READY_ON;
    GenCtrlState = GEN_STATE_START;
    TimerInState = TMR_START_TIMEOUT;
    INJ_Init();
}
static void ExecuteGenStateStart(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
    else{
    //InjectorEnabled = true; // Run injector always
    EnterGenStateRun(); //Enter Run mode
    }
}
static void EnterGenStateRun(void){
    InjectorEnabled = true;
    GenCtrlState = GEN_STATE_RUN;
}
static void ExecuteGenStateRun(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
}

static void EnterGenStateOff(void){
    CAG_DISABLE;
    InjectorEnabled = false;
    FUEL_PUMP_DISABLE;
    LSD_DISABLE;
    ChargeEnabled = true; // TODO: This is to allow external charging until "charger on" input is available
    LED_WAIT_OFF;
    LED_READY_OFF;
    LED_FAULT_ON;
    GenCtrlState = GEN_STATE_OFF;
}
static void ExecuteGenStateOff(void){
    ; // Stay here until reset
}


static void EnterGenStateCal(void){
    ChargeEnabled = true;  //why isnt this working???
    LSD_ENABLE;
    FUEL_PUMP_ENABLE;
    BDCFuelPumpEnable();
    RecalibrateTPS();
    InjectorEnabled = true;
    CAG_ENABLE;

    LED_WAIT_ON;
    LED_READY_ON;
    CalibrationMode = true;
    GenCtrlState = GEN_STATE_CAL;
}
static void ExecuteGenStateCal(void){
    if(GenCtrlFaultCode){
        EnterGenStateOff();
        return;
    }
}

void GenCtrlFSM(void){
    switch (GenCtrlState) {
        case GEN_STATE_INIT:
            ExecuteGenStateInit();
            break;
        case GEN_STATE_DRAIN:
            ExecuteGenStateDrain();
            break;
        case GEN_STATE_WAIT:
            ExecuteGenStateWait();
            break;
        case GEN_STATE_START:
            ExecuteGenStateStart();
            break;
        case GEN_STATE_RUN:
            ExecuteGenStateRun();
            break;
        case GEN_STATE_CAL:
            ExecuteGenStateCal();
            break;
        case GEN_STATE_OFF:
            ExecuteGenStateOff();
            break;
        default:
            ExecuteGenStateOff();
            break;
    }
}

/* Called from high priority ISR */
void GetIgnitionPeriod(void){
    // ReadECapture1(void) reads the ECCPR1L and ECCPR1H
    // Timer3 increments at 32.768 KHz
    IGN_PulseTimeNew = ReadECapture1();
    if(IGN_PulseTimeOld > 0){
        IGN_PulsePeriod = IGN_PulseTimeNew - IGN_PulseTimeOld;
    }
    IGN_PulseTimeOld = IGN_PulseTimeNew;
    // Validate pulse period (engine speed)
    if((IGN_PulsePeriod > IGN_PULSE_PERIOD_MIN) && (IGN_PulsePeriod < IGN_PULSE_PERIOD_MAX)){ // up to 125 mS
        IGN_PulseTimeoutCntr = TMR_IGN_PULSE_TIMEOUT;
    }else{
        //IGN_PulsePeriod = 0;
        IGN_PulsePeriod = 6000;
    }
}

// Pump with brushed DC motor
// TODO: Replace binary numbers with constants
void BDCFuelPumpEnable(void){
    // uint16_t buffer = 0;
    // uint8_t dummyread =0;
    // uint16_t PWM_FrequencyDytyCycle = 0b1010011110011001; // 25% 20*0.25 = 5V
    uint16_t PWM_FrequencyDytyCycle = 0b1010011110100001; // 33% 20*0.25 = 5V
    uint16_t PWM_Enable = 0b0001111100001111;
    uint16_t PWM_Output = 0b0011000000100000;

    SPI_SS_LSD_ON;
    SPI_Write16(PWM_FrequencyDytyCycle, SPI1);
    SPI_SS_LSD_OFF;

    __delay_us(100);
    SPI_SS_LSD_ON;
    SPI_Write16(PWM_Enable, SPI1);
    SPI_SS_LSD_OFF;

    __delay_us(100);
    SPI_SS_LSD_ON;
    SPI_Write16(PWM_Output, SPI1);
    SPI_SS_LSD_OFF;

    // Set PWM frequency and DC
    // All 0s returned by 33810 indicate no fault
    /*
    dummyread = MC33810_PWMxFrequencyDutyCycleCommand(&buffer, 0b0001100100); // PWM0_ADDRESS[2] | PWM_FREQUENCY_1_28HZ[3] | DC[7]
    // Set mode select mask to GP. Does not write to SPI
    MC33810_ModeSelect_IGN_GP(MODE_SELECT_GENERAL_PURPOSE_MODE_GD0
                           | MODE_SELECT_GENERAL_PURPOSE_MODE_GD1
                           | MODE_SELECT_GENERAL_PURPOSE_MODE_GD2
                           | MODE_SELECT_GENERAL_PURPOSE_MODE_GD3);
    // Enable PWM Mode write to SPI
    dummyread = MC33810_PWM_Enable(&buffer, MODE_SELECT_PWM3_ENABLE
                                           |MODE_SELECT_PWM2_ENABLE
                                           |MODE_SELECT_PWM1_ENABLE
                                           |MODE_SELECT_PWM0_ENABLE
                                           );
     */
}
// Disable open load fault detection and set short circuit detection to Tlim only
// TODO: Replace bin with constants
void LSD_FaultCommand(void){
    // Control Address 15,14,13,12 = 0010 (LSD Fault Command)
    // LSD Flt Operation 11,10,9 = 110 (Tlim Only)
    // Not used 8
    // 7 OUT3 ON Open Load detection
    // 6 OUT2 ON Open Load detection
    // 5 OUT1 ON Open Load detection
    // 4 OUT0 ON Open Load detection
    // 3 OUT3 OFF Open Load detection
    uint16_t faultCommand = 0b0010110000000000;

    SPI_SS_LSD_ON;
    SPI_Write16(faultCommand, SPI1);
    SPI_SS_LSD_OFF;
}
// Set time step and wait times based on the block temperature and fuel type.
// Called on startup only.
void BlockTempToTimeInit(void){
    // FIXME: Following is from Jason's old PCB code

   BlockTemp = EngTempADC_Filtered.Value * .4817 - 48.7; //celcius

    if(BlockTemp < -15.0){ //
        TimeStep = 60;
        TimeWait = 3500u; //35 seconds
    }else if(BlockTemp < -10.0){ //<-10 degrees  C
        TimeStep = 60;
        TimeWait = 3500u; //35 seconds
    }else if(BlockTemp < -5.0){ //<-5 degrees  C
        TimeStep = 55;
        TimeWait = 3000u; //30 seconds
    }else if(BlockTemp < 0.0){ //<0 degrees  C
        TimeStep = 50;

        TimeWait = 3000u; //30 seconds
    }else if(BlockTemp < 5.0){ //<5 degrees  C
        TimeStep = 45;
        TimeWait = 2000u; //20 seconds
    }else if(BlockTemp < 15.0){ //< 15 degrees
        TimeStep = 40;
        TimeWait = 1800u; //18 seconds
    }else if(BlockTemp < 20.0){ //< 20 degrees
        TimeStep = 30;
        TimeWait = 1600u; //16 seconds
    }else if(BlockTemp < 30.0){ //< 30 degrees
        TimeStep = 25;
        TimeWait = 1500u; //15 seconds
    }else if(BlockTemp < 40.0){ //<~40 degrees
        TimeStep = 20;
        TimeWait = 1200u; //12 seconds
    }else if(BlockTemp < 50.0){ //<~50 degrees
        TimeStep = 12;
        TimeWait = 800u; //8 seconds
    }else if(BlockTemp < 65.0){ //~60 degrees in 9fa
        TimeStep = 6;
        TimeWait = 300u; //3 seconds
    }else if(BlockTemp < 80.0){ // 70 degrees  in 9fa
       // TimeStep = (uint16_t)(0.2); // FIXME: This doesn't work, convert time to fp or eliminate
        TimeStep=2;
        TimeWait = 10u; //.1 seconds
    }else{ //above 80 degrees
       // TimeStep = (uint16_t)(0.2); // FIXME: This doesn't work, convert time to fp or eliminate
         TimeStep=1;
        TimeWait = 10u; //.1 seconds

    }



    // TODO: Select TimeStep and TimeWait for different fuel types
    if(FuelType == FUEL_TYPE_GAS){
        TimeStep = (uint16_t)(0.2); // FIXME: This doesn't work, convert time to fp or eliminate
        TimeWait = 10;
    }
}
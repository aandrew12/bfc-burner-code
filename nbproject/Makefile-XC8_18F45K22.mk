#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-XC8_18F45K22.mk)" "nbproject/Makefile-local-XC8_18F45K22.mk"
include nbproject/Makefile-local-XC8_18F45K22.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=XC8_18F45K22
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=configuration_bits.c interrupts.c main.c system.c scheduler.c RS232.c peripherals.c injector.c linear_interp.c sqrt_fixed_point.c control.c battery.c dac.c atemp.c etemp.c calcfueltypeparameter.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/configuration_bits.p1 ${OBJECTDIR}/interrupts.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/system.p1 ${OBJECTDIR}/scheduler.p1 ${OBJECTDIR}/RS232.p1 ${OBJECTDIR}/peripherals.p1 ${OBJECTDIR}/injector.p1 ${OBJECTDIR}/linear_interp.p1 ${OBJECTDIR}/sqrt_fixed_point.p1 ${OBJECTDIR}/control.p1 ${OBJECTDIR}/battery.p1 ${OBJECTDIR}/dac.p1 ${OBJECTDIR}/atemp.p1 ${OBJECTDIR}/etemp.p1 ${OBJECTDIR}/calcfueltypeparameter.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/configuration_bits.p1.d ${OBJECTDIR}/interrupts.p1.d ${OBJECTDIR}/main.p1.d ${OBJECTDIR}/system.p1.d ${OBJECTDIR}/scheduler.p1.d ${OBJECTDIR}/RS232.p1.d ${OBJECTDIR}/peripherals.p1.d ${OBJECTDIR}/injector.p1.d ${OBJECTDIR}/linear_interp.p1.d ${OBJECTDIR}/sqrt_fixed_point.p1.d ${OBJECTDIR}/control.p1.d ${OBJECTDIR}/battery.p1.d ${OBJECTDIR}/dac.p1.d ${OBJECTDIR}/atemp.p1.d ${OBJECTDIR}/etemp.p1.d ${OBJECTDIR}/calcfueltypeparameter.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/configuration_bits.p1 ${OBJECTDIR}/interrupts.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/system.p1 ${OBJECTDIR}/scheduler.p1 ${OBJECTDIR}/RS232.p1 ${OBJECTDIR}/peripherals.p1 ${OBJECTDIR}/injector.p1 ${OBJECTDIR}/linear_interp.p1 ${OBJECTDIR}/sqrt_fixed_point.p1 ${OBJECTDIR}/control.p1 ${OBJECTDIR}/battery.p1 ${OBJECTDIR}/dac.p1 ${OBJECTDIR}/atemp.p1 ${OBJECTDIR}/etemp.p1 ${OBJECTDIR}/calcfueltypeparameter.p1

# Source Files
SOURCEFILES=configuration_bits.c interrupts.c main.c system.c scheduler.c RS232.c peripherals.c injector.c linear_interp.c sqrt_fixed_point.c control.c battery.c dac.c atemp.c etemp.c calcfueltypeparameter.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-XC8_18F45K22.mk dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F45K22
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/configuration_bits.p1: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configuration_bits.p1.d 
	@${RM} ${OBJECTDIR}/configuration_bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/configuration_bits.p1 configuration_bits.c 
	@-${MV} ${OBJECTDIR}/configuration_bits.d ${OBJECTDIR}/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interrupts.p1: interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/interrupts.p1 interrupts.c 
	@-${MV} ${OBJECTDIR}/interrupts.d ${OBJECTDIR}/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/system.p1: system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/system.p1.d 
	@${RM} ${OBJECTDIR}/system.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/system.p1 system.c 
	@-${MV} ${OBJECTDIR}/system.d ${OBJECTDIR}/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/scheduler.p1: scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/scheduler.p1.d 
	@${RM} ${OBJECTDIR}/scheduler.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/scheduler.p1 scheduler.c 
	@-${MV} ${OBJECTDIR}/scheduler.d ${OBJECTDIR}/scheduler.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/scheduler.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/RS232.p1: RS232.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/RS232.p1.d 
	@${RM} ${OBJECTDIR}/RS232.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/RS232.p1 RS232.c 
	@-${MV} ${OBJECTDIR}/RS232.d ${OBJECTDIR}/RS232.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/RS232.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/peripherals.p1: peripherals.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/peripherals.p1.d 
	@${RM} ${OBJECTDIR}/peripherals.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/peripherals.p1 peripherals.c 
	@-${MV} ${OBJECTDIR}/peripherals.d ${OBJECTDIR}/peripherals.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/peripherals.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/injector.p1: injector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/injector.p1.d 
	@${RM} ${OBJECTDIR}/injector.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/injector.p1 injector.c 
	@-${MV} ${OBJECTDIR}/injector.d ${OBJECTDIR}/injector.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/injector.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/linear_interp.p1: linear_interp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/linear_interp.p1.d 
	@${RM} ${OBJECTDIR}/linear_interp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/linear_interp.p1 linear_interp.c 
	@-${MV} ${OBJECTDIR}/linear_interp.d ${OBJECTDIR}/linear_interp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/linear_interp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sqrt_fixed_point.p1: sqrt_fixed_point.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sqrt_fixed_point.p1.d 
	@${RM} ${OBJECTDIR}/sqrt_fixed_point.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sqrt_fixed_point.p1 sqrt_fixed_point.c 
	@-${MV} ${OBJECTDIR}/sqrt_fixed_point.d ${OBJECTDIR}/sqrt_fixed_point.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sqrt_fixed_point.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/control.p1: control.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/control.p1.d 
	@${RM} ${OBJECTDIR}/control.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/control.p1 control.c 
	@-${MV} ${OBJECTDIR}/control.d ${OBJECTDIR}/control.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/control.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/battery.p1: battery.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/battery.p1.d 
	@${RM} ${OBJECTDIR}/battery.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/battery.p1 battery.c 
	@-${MV} ${OBJECTDIR}/battery.d ${OBJECTDIR}/battery.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/battery.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/dac.p1: dac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/dac.p1.d 
	@${RM} ${OBJECTDIR}/dac.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/dac.p1 dac.c 
	@-${MV} ${OBJECTDIR}/dac.d ${OBJECTDIR}/dac.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/dac.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/atemp.p1: atemp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/atemp.p1.d 
	@${RM} ${OBJECTDIR}/atemp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/atemp.p1 atemp.c 
	@-${MV} ${OBJECTDIR}/atemp.d ${OBJECTDIR}/atemp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/atemp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/etemp.p1: etemp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/etemp.p1.d 
	@${RM} ${OBJECTDIR}/etemp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/etemp.p1 etemp.c 
	@-${MV} ${OBJECTDIR}/etemp.d ${OBJECTDIR}/etemp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/etemp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/calcfueltypeparameter.p1: calcfueltypeparameter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/calcfueltypeparameter.p1.d 
	@${RM} ${OBJECTDIR}/calcfueltypeparameter.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1  --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/calcfueltypeparameter.p1 calcfueltypeparameter.c 
	@-${MV} ${OBJECTDIR}/calcfueltypeparameter.d ${OBJECTDIR}/calcfueltypeparameter.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/calcfueltypeparameter.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/configuration_bits.p1: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/configuration_bits.p1.d 
	@${RM} ${OBJECTDIR}/configuration_bits.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/configuration_bits.p1 configuration_bits.c 
	@-${MV} ${OBJECTDIR}/configuration_bits.d ${OBJECTDIR}/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interrupts.p1: interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupts.p1.d 
	@${RM} ${OBJECTDIR}/interrupts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/interrupts.p1 interrupts.c 
	@-${MV} ${OBJECTDIR}/interrupts.d ${OBJECTDIR}/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/system.p1: system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/system.p1.d 
	@${RM} ${OBJECTDIR}/system.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/system.p1 system.c 
	@-${MV} ${OBJECTDIR}/system.d ${OBJECTDIR}/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/scheduler.p1: scheduler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/scheduler.p1.d 
	@${RM} ${OBJECTDIR}/scheduler.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/scheduler.p1 scheduler.c 
	@-${MV} ${OBJECTDIR}/scheduler.d ${OBJECTDIR}/scheduler.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/scheduler.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/RS232.p1: RS232.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/RS232.p1.d 
	@${RM} ${OBJECTDIR}/RS232.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/RS232.p1 RS232.c 
	@-${MV} ${OBJECTDIR}/RS232.d ${OBJECTDIR}/RS232.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/RS232.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/peripherals.p1: peripherals.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/peripherals.p1.d 
	@${RM} ${OBJECTDIR}/peripherals.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/peripherals.p1 peripherals.c 
	@-${MV} ${OBJECTDIR}/peripherals.d ${OBJECTDIR}/peripherals.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/peripherals.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/injector.p1: injector.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/injector.p1.d 
	@${RM} ${OBJECTDIR}/injector.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/injector.p1 injector.c 
	@-${MV} ${OBJECTDIR}/injector.d ${OBJECTDIR}/injector.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/injector.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/linear_interp.p1: linear_interp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/linear_interp.p1.d 
	@${RM} ${OBJECTDIR}/linear_interp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/linear_interp.p1 linear_interp.c 
	@-${MV} ${OBJECTDIR}/linear_interp.d ${OBJECTDIR}/linear_interp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/linear_interp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sqrt_fixed_point.p1: sqrt_fixed_point.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/sqrt_fixed_point.p1.d 
	@${RM} ${OBJECTDIR}/sqrt_fixed_point.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/sqrt_fixed_point.p1 sqrt_fixed_point.c 
	@-${MV} ${OBJECTDIR}/sqrt_fixed_point.d ${OBJECTDIR}/sqrt_fixed_point.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sqrt_fixed_point.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/control.p1: control.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/control.p1.d 
	@${RM} ${OBJECTDIR}/control.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/control.p1 control.c 
	@-${MV} ${OBJECTDIR}/control.d ${OBJECTDIR}/control.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/control.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/battery.p1: battery.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/battery.p1.d 
	@${RM} ${OBJECTDIR}/battery.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/battery.p1 battery.c 
	@-${MV} ${OBJECTDIR}/battery.d ${OBJECTDIR}/battery.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/battery.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/dac.p1: dac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/dac.p1.d 
	@${RM} ${OBJECTDIR}/dac.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/dac.p1 dac.c 
	@-${MV} ${OBJECTDIR}/dac.d ${OBJECTDIR}/dac.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/dac.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/atemp.p1: atemp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/atemp.p1.d 
	@${RM} ${OBJECTDIR}/atemp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/atemp.p1 atemp.c 
	@-${MV} ${OBJECTDIR}/atemp.d ${OBJECTDIR}/atemp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/atemp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/etemp.p1: etemp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/etemp.p1.d 
	@${RM} ${OBJECTDIR}/etemp.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/etemp.p1 etemp.c 
	@-${MV} ${OBJECTDIR}/etemp.d ${OBJECTDIR}/etemp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/etemp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/calcfueltypeparameter.p1: calcfueltypeparameter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/calcfueltypeparameter.p1.d 
	@${RM} ${OBJECTDIR}/calcfueltypeparameter.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist -DXPRJ_XC8_18F45K22=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     -o${OBJECTDIR}/calcfueltypeparameter.p1 calcfueltypeparameter.c 
	@-${MV} ${OBJECTDIR}/calcfueltypeparameter.d ${OBJECTDIR}/calcfueltypeparameter.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/calcfueltypeparameter.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.map  -D__DEBUG=1  --debugger=none  -DXPRJ_XC8_18F45K22=$(CND_CONF)  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"        $(COMPARISON_BUILD)  -odist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.map  -DXPRJ_XC8_18F45K22=$(CND_CONF)  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,+speed,-space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.30/include/plib" --warn=0 --cci --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     $(COMPARISON_BUILD)  -odist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
	@echo "Calculating checksum"
	@${MP_JAVA_PATH}java -jar "C:/Microchip/mplab_platform/platform/../mplab_ide/modules/../../bin/checksum.jar" -i PIC18F45K22 dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.hex 2> NUL 1> NUL

	@echo Normalizing hex file
	@"C:/Microchip/mplab_platform/platform/../mplab_ide/modules/../../bin/hexmate" --edf="C:/Microchip/mplab_platform/platform/../mplab_ide/modules/../../dat/en_msgs.txt" dist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.hex -odist/${CND_CONF}/${IMAGE_TYPE}/bfc-burner-code.${IMAGE_TYPE}.hex

endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/XC8_18F45K22
	${RM} -r dist/XC8_18F45K22

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif

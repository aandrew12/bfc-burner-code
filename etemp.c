/*
 * File:   etemp.h
 * Author: dyulov
 *
 * Created on November 7, 2013, 11:47 AM
 *
 * Engine Temperature Monitoring
 */

// TODO: Add better signal validation
#include <xc.h>
// Standard library headersb
#include <stdint.h>
#include <stdbool.h>
#include "types.h"
#include "peripherals.h"
#include "etemp.h"
#include "CalcFuelTypeParameter.h"

#define ETEMP_BUFFER_SIZE 16
extern uint16_t SecondsRunTime;
AnalogSignal_t EngTempADC_Filtered;
// AnalogSignal_t Tps_Filtered;

uint16_t EngTempDataBuffer[ETEMP_BUFFER_SIZE];
// uint16_t TpsDataBuffer[ETEMP_BUFFER_SIZE];
uint8_t EngTempBufferCounter = 0;
// uint8_t TpsBufferCounter=0;
extern uint16_t TempSet;
int testflag=2;
int counter = 0;
int shutter_counter=0;
int EngTempError=0;
int AbsEngTempError=0;
int n=0;

void EngTempFilter(void);

void EngTempInit(void){
    uint8_t bufferIndex;
    for(bufferIndex = 0; bufferIndex < ETEMP_BUFFER_SIZE; bufferIndex++){
        EngTempDataBuffer[bufferIndex] = 0;
    }
    EngTempADC_Filtered.Value = 0;
    EngTempADC_Filtered.IsValid = false;
}

void EngTempMonitor(void){
    EngTempDataBuffer[EngTempBufferCounter] = ADC_Convert(ADC_CH3); // Get air temperature
    EngTempBufferCounter++;
    if(EngTempBufferCounter == ETEMP_BUFFER_SIZE){
        EngTempBufferCounter = 0;
        EngTempFilter();
    }

   }

void EngTempControl(void){
    //EngTempADC_Filtered.Value * .4817 - 48.7
    //EngTempADC_Filtered.Value
// this function moves a brushed dc motor that automates the shutter.
// two low side outputs: "LSD_SPARE" and "OIL_PUMP"  are used to supply eithe leg of the motor with ground, both legs of the motor are always powere up with battery volatag through a resistor

    if (SecondsRunTime==300 || SecondsRunTime==600){
        shutter_counter=0;
    }

    EngTempError=EngTempADC_Filtered.Value-TempSet;  //control to 76 on 2000
    if (EngTempError<0){
        AbsEngTempError=0-EngTempError;
    }
    else {AbsEngTempError=EngTempError;
    }
    if(AbsEngTempError<5){
        n=500;  //n
        }
    if(AbsEngTempError<10){
        n=100;  //n
        }
        else if (AbsEngTempError<20){
        n=10;
        }
        else{
        n=5;
        }





   /* if (shutter_counter>10){
        shutter_counter=10;
    }
    if (shutter_counter<-10){
        shutter_counter=-10;
    }*/



   /*test section
     * if(testflag>0){
            LSD_SPARE_DISABLE;
            OIL_PUMP_ENABLE;
            testflag=testflag-1;

    }
    if(testflag==0){
            LSD_SPARE_ENABLE;
            OIL_PUMP_DISABLE;
            testflag=testflag+2;

    }*/
    // this section disables the outputs therefore stopping the motor to provide some hysterises ~ every 10 sec
    if (counter <n){
    LSD_SPARE_DISABLE;
    OIL_PUMP_DISABLE;
    }
     counter=counter+1;
    if (counter >n-1) {
        counter++;
         if((EngTempError>0) && (shutter_counter<120)){  //too hot, open shutter  272=82.3
    LSD_SPARE_DISABLE;
    OIL_PUMP_ENABLE;
    shutter_counter++;
    if (ADC_Convert(ADC_CH2)<200&&(shutter_counter>2))
       
    {shutter_counter=120;

    }
    }
         else if((EngTempError<0) && (shutter_counter>-120)){  //too cold, close shutter
    LSD_SPARE_ENABLE;

    OIL_PUMP_DISABLE;
    shutter_counter--;
    if (ADC_Convert(ADC_CH2)<200&&(shutter_counter<-2))
    {shutter_counter=-120;

    }
    }

         if (counter>n+2){
        counter = 0;}}
}


void EngTempFilter(void){
    uint8_t bufferIndex;
    uint16_t engTempAvg = 0;
   // uint16_t TpsAvg =0;
    for(bufferIndex = 0; bufferIndex < ETEMP_BUFFER_SIZE; bufferIndex++){
        engTempAvg += EngTempDataBuffer[bufferIndex];
      //  TpsAvg+=TpsDataBuffer[bufferIndex];
    }
    engTempAvg = engTempAvg >> 4;
   // TpsAvg = TpsAvg >> 4;
   // Tps_Filtered.Value=TpsAvg;
    EngTempADC_Filtered.Value = engTempAvg;
    EngTempADC_Filtered.IsValid = true;
   // Tps_Filtered.IsValid=true;
}


/* 
 * File:   CalcFuelTypeParameter.h
 * Author: labrat
 *
 * Created on March 28, 2016, 12:03 PM
 */

/*
 * File:   etemp.h
 * Author: dyulov
 *
 * Created on November 7, 2013, 11:47 AM
 */

#ifndef CALCFUELTYPEPARAMETER_H
#define	CALCFUELTYPEPARAMETER_H

#ifdef	__cplusplus
extern "C"{
#endif

void TimeMultInit(void);
void CagOffsetInit(void);
void TempParametersInit(void);

#ifdef	__cplusplus
}
#endif

#endif	/* ETEMP_H */


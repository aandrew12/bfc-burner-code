#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>

#include "system.h"

IntOSCFreq ClockSpeed = C_64MHz;
void SetIntOSC(IntOSCFreq ClockSet);

/* Refer to the device datasheet for information about available
oscillator configurations. */
void ConfigureOscillator(void)
{
    /* TODO Add clock switching code if appropriate.  */
    SetIntOSC(ClockSpeed);
    /* Typical actions in this function are to tweak the oscillator tuning
    register, select new clock sources, and to wait until new clock sources
    are stable before resuming execution of the main project. */
}

void SetIntOSC(IntOSCFreq ClockSet)
{   // This function sets the internal oscillator to the frequency of
    // the ClockSet argument variable
    switch (ClockSet)
    {
        case C_250kHz:
            OSCCON = 0x10;          // IRCFx = 001
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;

        case C_500kHz:
            OSCCON = 0x20;          // IRCFx = 010
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;

        case C_1MHz:
            OSCCON = 0x30;          // IRCFx = 011
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;

        case C_2MHz:
            OSCCON = 0x40;          // IRCFx = 100
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;

        case C_4MHz:
            OSCCON = 0x50;          // IRCFx = 101
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;

        case C_8MHz:
            OSCCON = 0x60;          // IRCFx = 110
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;

        case C_16MHz:
            OSCCON = 0x70;          // IRCFx = 111
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;

        case C_32MHz:
            OSCCON = 0x60;          // IRCFx = 110 (8 MHz)
            OSCTUNEbits.PLLEN = 1;  // x4 PLL enabled = 32MHz
            break;

        case C_64MHz:
            OSCCON = 0x70;          // IRCFx = 111 (16 MHz)
            OSCTUNEbits.PLLEN = 1;  // x4 PLL enabled = 64MHz
            break;

        default:
            // should never get here, but just in case
            OSCCON = 0x10;          // IRCFx = 001
            OSCTUNEbits.PLLEN = 0;  // x4 PLL disabled
            break;
    }
}

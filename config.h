

/*
 * File:   config.h
 * Author: dyulov
 *
 * Created on October 22, 2013, 9:40 AM
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C"{
#endif

// TPS calibration parameters
#define ENGINE_NUMBER 1024//DONT FORGET to change Dual Slope constants when switching from 1 to 2 kW engines
#if (ENGINE_NUMBER == 1004) // Honda EU1000i
#define TPS_VOLT_LO PU_16S_CONVERT(.5, V_REF, 1024)  //10-23 from 1.18 //9-27 moved to 1007, formerly 1.11, engine 4 Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.5, V_REF, 1024) //10-23 from 4.57 //9-27 moved to 1007, formerly 4.62
#elif (ENGINE_NUMBER == 1005) // Honda EU1000i
#define TPS_VOLT_LO PU_16S_CONVERT(1.68, V_REF, 1024) // engine 6 aka 1005 Closed throttle pushed all the way- TPS lower voltage wa 1.33 until 4/3, then 1.23 until 4/15 pushed all the way
#define TPS_VOLT_HI PU_16S_CONVERT(4.8, V_REF, 1024) //engine 6 aka 1005  WOT - TPS upper voltage  - was 4.33 unitl 4/
#elif (ENGINE_NUMBER == 1007) // Honda EU1000i
#define TPS_VOLT_LO PU_16S_CONVERT(1.11, V_REF, 1024) // engine 1007 Closed throttle pushed all the way- TPS lower voltage wa 1.33 until 4/3, then 1.23 until 4/15 pushed all the way
#define TPS_VOLT_HI PU_16S_CONVERT(4.62, V_REF, 1024) //engine 1007  WOT - TPS upper voltage  - was 4.33 unitl 4/3
#elif (ENGINE_NUMBER == 1009) // Honda EU1000i
#define TPS_VOLT_LO PU_16S_CONVERT(1.26, V_REF, 1024) // engine 1009 Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.0, V_REF, 1024) //engine 1009  WOT - TPS upper voltage
#elif (ENGINE_NUMBER == 1010) // Honda EU1000i
#define TPS_VOLT_LO PU_16S_CONVERT(1.33, V_REF, 1024) // engine 1010 Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.40, V_REF, 1024) //engine 1010  WOT - TPS upper voltage
#elif (ENGINE_NUMBER == 1011) // Honda EU1000i
#define TPS_VOLT_LO PU_16S_CONVERT(1.37, V_REF, 1024) // engine 1011 Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.38, V_REF, 1024) //engine 1011  WOT - TPS upper voltage
#elif (ENGINE_NUMBER == 1012) // Honda EU1000i  //first Fidelity engine 2/6/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.47, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.4, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
    #elif (ENGINE_NUMBER == 1013) // Honda EU1000i  //second Fidelity engine 2/6/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.45, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.38, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
 #elif (ENGINE_NUMBER == 1014) // Honda EU1000i  //third Fidelity engine 2/6/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.43, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.48, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
 #elif (ENGINE_NUMBER == 1015) // Honda EU1000i  //fourth Fidelity engine 2/6/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.50, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.42, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
#elif (ENGINE_NUMBER == 1016) // Honda EU1000i  //fifth Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.43, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.45, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
 #elif (ENGINE_NUMBER == 1017) // Honda EU1000i  //sixth Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.39, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.44, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
 #elif (ENGINE_NUMBER == 1018) // Honda EU1000i  //seventh Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.45, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.35, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
#elif (ENGINE_NUMBER == 1019) // Honda EU1000i  //seventh Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.5, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.5, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
#elif (ENGINE_NUMBER == 1020) // Honda EU1000i  //seventh Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.5, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.5, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
#elif (ENGINE_NUMBER == 1021) // Honda EU1000i  //seventh Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(0.5, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.5, V_REF, 1024) //engine 2009  WOT - TPS upper voltage
#elif (ENGINE_NUMBER > 1020 && ENGINE_NUMBER<2000) // Honda EU1000i
#define TPS_VOLT_LO PU_16S_CONVERT(0.5, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.5, V_REF, 1024) //engine 2009  WOT - TPS upper voltage


#elif (ENGINE_NUMBER == 2009) // Honda EU1000i  //seventh Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(1.12, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.44, V_REF, 1024) //engine 2009  WOT - TPS upper voltage

    #elif (ENGINE_NUMBER == 2008) // Honda EU1000i  //seventh Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(1.48, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.76, V_REF, 1024) //engine 2009  WOT - TPS upper voltage

     #elif (ENGINE_NUMBER ==2010) // Honda EU1000i  //seventh Fidelity engine 2/18/15
#define TPS_VOLT_LO PU_16S_CONVERT(1.45, V_REF, 1024) // engine 2009  Closed throttle - TPS lower voltage
#define TPS_VOLT_HI PU_16S_CONVERT(4.69, V_REF, 1024) //engine 2009  WOT - TPS upper voltage




#endif
#define INJ_DC_LO PU_16S_CONVERT(8, PERCENT_REF, R_PERCENT) // Closed throttle - Lower duty cycle changed from 30/70 2/28/14 for test
#define INJ_DC_HI PU_16S_CONVERT(10, PERCENT_REF, R_PERCENT) // 2/18/15 from 15 to address "chugging/puttering" //was 90 changed for test 4/4 WOT - Upper duty cycle -changed 7/10 from 40

    // Injector pulse width limits

#define INJ_DC_MIN PU_16S_CONVERT(11, PERCENT_REF, R_PERCENT) // Min on-time compatible with the injector
#define INJ_DC_MAX PU_16S_CONVERT(90, PERCENT_REF, R_PERCENT) // Min off-time compatible with the injector
// TPS input ranges for manual CAG calibration
#define CAL_TPS_VOLT_LO PU_16S_CONVERT(0, V_REF, 1024) // Closed throttle - TPS lower voltage
#define CAL_TPS_VOLT_HI PU_16S_CONVERT(5, V_REF, 1024) // WOT - TPS upper voltage
#define CAL_INJ_DC_LO PU_16S_CONVERT(8, PERCENT_REF, R_PERCENT) // Closed throttle - Lower duty   changed from 10% 2/28/14
#define CAL_INJ_DC_HI PU_16S_CONVERT(90, PERCENT_REF, R_PERCENT) // WOT - Upper duty cycle
// Temperature parameters
#define TEMP_SENSE_OFFSET (0.5) // MCP9700A-E 500mV @ 0C
#define TEMP_SENSE_GAIN (0.01) // MCP9700A-E 10mV/C
#define AIR_TEMP_COMP_EN 1 //0
#define ENG_TEMP_COMP_EN 0
#define AIR_TEMP_VOLT_LO PU_16S_CONVERT(0.1, V_REF, 1024)
#define AIR_TEMP_VOLT_HI PU_16S_CONVERT(2.0, V_REF, 1024)
#define AIR_TEMP_FM_LO PU_16S_CONVERT(120, PERCENT_REF, R_PERCENT)
#define AIR_TEMP_FM_HI PU_16S_CONVERT(50, PERCENT_REF, R_PERCENT)
// Battery parameters
#define VSENSE_GAIN (0.164) // Vsense Voltage divider ratio
#define BATT_VOLT_25C_LO PU_16S_CONVERT((15.5 * VSENSE_GAIN), V_REF, 1024) // 516 // 2.20V*7 15.40V -> 2.525V -> 516 // Start charging 50% SOC// changed from 15.4 for 75% SOC per 2014-06-02-OCV-SOC-DOD.xls
#define BATT_VOLT_25C_HI PU_16S_CONVERT((16.24 * VSENSE_GAIN), V_REF, 1024) // 587 // 2.50V*7 17.50v -> 2.870V -> 587 // Stop charging changed from 17.5 6/2 to go to 91.3% (4.
#define BATT_TEMP_LO PU_16S_CONVERT((-40.0 * TEMP_SENSE_GAIN + TEMP_SENSE_OFFSET), V_REF, 1024)
#define BATT_TEMP_HI PU_16S_CONVERT((125.0 * TEMP_SENSE_GAIN + TEMP_SENSE_OFFSET), V_REF, 1024)
#define BATT_TEMP_COMP_OFFSET_LO PU_16S_CONVERT((2.27 * VSENSE_GAIN), V_REF, 1024)
#define BATT_TEMP_COMP_OFFSET_HI PU_16S_CONVERT((-3.50 * VSENSE_GAIN), V_REF, 1024)

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */


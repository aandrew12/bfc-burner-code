/* 
 * File:   linear_interp.c
 */
// Standard library headers
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "fixed_point.h"
#include "linear_interp.h"

void LinearInterpInit(
        struct LINEAR_INTERP_STRUCT * d, ///< Linear Interp Structure
        int16_t x1,
        int16_t y1,
        int16_t x2,
        int16_t y2,
        int16_t q_slope ///< Q value for intermediate slope value (deltaY/deltaX) also used for output multiplication (slope * deltaX)
){
    bool calWarnDetected = false;
    d->q_slope = q_slope;

    if(x1<=x2){
        // inputs are defined correctly, assign as expected
        d->x1 = x1;
        d->x2 = x2;
        d->y1 = y1;
        d->y2 = y2;
    }else{
        // inputs were specified out of order, swap them so that x1 < x2 in the structure.
        d->x1 = x2;
        d->x2 = x1;
        d->y1 = y2;
        d->y2 = y1;

        // If x1 and x2 were specified out of order, this was probably not intended by user and could
        // have serious change in behavior (for example, if end points were specified as x1 and "ramp" and ramp was negative...
        calWarnDetected = true;
    }

    // assuming that using local variables is less overhead than using structure ones?
    if(x1 == x2){
        // This special case handles the divide by zero protection below and also
        // allows configuration to handle a step change.
#define INF 0x7FFF // for a int16_t
        d->slope = INF; //(y2>y1)? INF: -INF; // avoid divide by zero, value doesn't really matter, it won't be used.
#undef INF
    }else{

        //If ignore the overflow protection, just do this:
        //d->slope = Q_DIV( (y2-y1), (x2 - x1), q_slope);

        int16_t delta_y, delta_x, delta_x_abs, delta_y_abs;
        int16_t max_slope;
        delta_y = y2 - y1;
        delta_x = x2 - x1;
        max_slope = 32767;

        /*  Check for overflow in calculation of slope:

            OK if the following is true:
            -max_slope < dy/dx < max_slope

            -dx < dy/max_slope < dx
        */
        delta_x_abs = abs(delta_x);
        delta_y_abs = abs(delta_y);

        if((delta_y_abs>>(15-q_slope)) < delta_x_abs){

//      if( (( delta_x_abs* (-max_slope_q0)) < delta_y) && (delta_y <  (delta_x_abs * max_slope_q0)) ){
//          // overflow is not going to be a problem, but not checking that delta_x*max_slope is overflowing...
            d->slope = Q_DIV( delta_y, delta_x, q_slope);
        }else{
            calWarnDetected = true; // overflow adjustment applied.
            if(delta_y == 0){
                d->slope = 0; // handle this special case even in the case of "overflow"
            }else if(delta_y > 0){
                d->slope = max_slope;
            }else{
                d->slope = -max_slope;
            }
        }
    }
}

int16_t LinearInterp(
        struct LINEAR_INTERP_STRUCT * d, ///< Linear Interp Structure
        int16_t x ///< current value of x
){

    int16_t y;

    if(x <= d->x1){
        /* Below x1, Cap at y1 */
        // note if x1 == x2, y1 will be used.
        y = d->y1;
    }else if(x >= d->x2 ){
        /* Above x2, Cap at y2 */
        y = d->y2;
    }else{
        // y = y1 + (x - x1)*dy/dx
        y = d->y1 + Q_MUL( (x - d->x1), d->slope, d->q_slope);
    }
    return(y);
}



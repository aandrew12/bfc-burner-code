/*
 * File:   injector.c
 * Author: dyulov
 *
 * Created on April 16, 2013, 11:59 PM
 */
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>
#include "fixed_point.h"
#include "peripherals.h"
#include "linear_interp.h"
#include "sqrt_fixed_point.h"
#include "control.h"
#include "config.h"
#include "atemp.h"
#include "etemp.h"
#include "types.h"
#include "scheduler.h"
#include "injector.h"


extern FuelType_t FuelType;
extern bool InjectorEnabled;
extern int16_t TimeStep;  //from uint16_t
extern uint8_t CAG_Setpoint;
extern int16_t BlockTempADC;  //???? Do I need this?
extern AnalogSignal_t EngTempADC_Filtered;
extern uint16_t CAG_Offset;
//uint16_t CAG_Offset_Limited;
uint16_t TempIn;

int16_t PurgeTime = 1;
uint16_t TimeSec = 1;

extern uint16_t PWM1_DutyCycle;
extern void BDCFuelPumpEnable(void);

extern float BlockTemp;  //???? Do I need this?
extern bool CalibrationMode;
extern AnalogSignal_t AirTempADC_Filtered;

int16_t TPS_VoltLo = TPS_VOLT_LO; // interp x1
int16_t TPS_VoltHi = TPS_VOLT_HI; // interp x2

int16_t INJ_DutyCycleLo = INJ_DC_LO; // interp y1
int16_t INJ_DutyCycleHi = INJ_DC_HI; // interp y2

int16_t INJ_ThrottleADC = 0;
int16_t INJ_PressureADC = 0;
int16_t INJ_DutyCycle = 0;
int16_t INJ_DutyCycle_old = 70;
int16_t INJ_ThrottleADC_old = 0;

double FuelModifier = 1.0;
//CHANGE BY AA TO EXTERN 9/8/2017
extern uint16_t SecondsRunTime = 0;

uint16_t nextstep = 0;
uint16_t CountEnrichment = 10000;
uint16_t hot_flag=0;
int16_t INJ_Pot = 0;
float PotMult=1.0;

int DC_constant=300;
//uint16_t CAG_Offset_ss=506;

// FIXME: From Jason's old PCB code
double t_ambient = 20.0;
double rho_multiplier = 1.0; //density multiplier based on temperature

struct LINEAR_INTERP_STRUCT TPS_InjPWMInterp;
bool UsePot = false;
uint16_t StartupDC = 2; //this was 0....is this what starts the small pulse at beginning?

struct LINEAR_INTERP_STRUCT FM_AirTempInterp;
struct LINEAR_INTERP_STRUCT PotInterp;

void INJ_StartSequence(void);

// Call to use values for manual CAG calibration
void RecalibrateTPS(void){
    TPS_VoltLo = CAL_TPS_VOLT_LO; // interp x1
    TPS_VoltHi = CAL_TPS_VOLT_HI; // interp x2
    INJ_DutyCycleLo = CAL_INJ_DC_LO; // interp y1
    INJ_DutyCycleHi = CAL_INJ_DC_HI; // interp y2
    INJ_Init();
}


void INJ_FuelModifierCalc(void) {
    FuelModifier = 1.0;
}

void INJ_Init(void){
    LinearInterpInit(
        &TPS_InjPWMInterp,
        TPS_VoltLo, INJ_DutyCycleLo,
        TPS_VoltHi, INJ_DutyCycleHi,
        Q_PERCENT);
    LinearInterpInit(
        &FM_AirTempInterp,
        AIR_TEMP_VOLT_LO, AIR_TEMP_FM_LO,
        AIR_TEMP_VOLT_HI, AIR_TEMP_FM_HI,
        Q_PERCENT);
    INJ_FuelModifierCalc();
    SCH_AddTask(INJ_FuelModifierCalc, 0, 100);
    PWM_LS_Init(); // Start INJ PWM output
}

/* Called from high priority ISR */
void INJ_SetDutyCycle(void){
    
    //INJ_ThrottleADC_old=INJ_ThrottleADC;
    //INJ_ThrottleADC = ADC_Convert(ADC_CH7); // Get 10-bit TPS position
    INJ_DutyCycle_old=INJ_DutyCycle;

    // Clear CCP4CON register and reload it with compare mode
    CCP4CON = 0x0;
    // Copy 10-bit +DC value to 16-bit CCPRxH:CCPRxL register pair
    CCPR4L = (INJ_DutyCycle << 6) & 0xFF;
    CCPR4H = (INJ_DutyCycle >> 2) & 0xFF;

    if(InjectorEnabled){
        CCP4CON = 0b00001001; // CCP1CONbits.CCP4M = ECOM_LO_MATCH, Init RD1 Hi, force Lo on compare match
        if(!CalibrationMode){
            INJ_StartSequence();
        }
    }else{
        SecondsRunTime = 0; // Fueling stopped - reset sw timer
    }
}

// TODO: This function is for test only - remove or correct interval value
/* Call from injector timer interrupt at 16 mS intervals */
void INJ_StartSequence(void){
    
    static uint16_t interval = 0;
    interval++;
    
    if(interval>=62){
       SecondsRunTime++;
       interval = 0;
    }

         if (SecondsRunTime<3){
            CAG_Offset=580;
            INJ_DutyCycle=10;

          }
            else if (SecondsRunTime<4){
         CAG_Offset=560;
         INJ_DutyCycle=1020;

          }

            else if (SecondsRunTime<5){
         CAG_Offset=550;
         INJ_DutyCycle=900;

          }

            else if (SecondsRunTime<6){
         CAG_Offset=545;
         INJ_DutyCycle=900;

          }

            else if (SecondsRunTime<10){
         CAG_Offset=536;
         INJ_DutyCycle=900;

          }

            else if (SecondsRunTime<15){
         CAG_Offset=536;
         INJ_DutyCycle=900;

          }

            else if (SecondsRunTime<20){
         CAG_Offset=536;
         INJ_DutyCycle=900;

          }

            else if (SecondsRunTime<25){
         CAG_Offset=526;
         INJ_DutyCycle=900;

          }

            else if (SecondsRunTime<30){
         CAG_Offset=526;
         INJ_DutyCycle=900;

          }

      else  {
           CAG_Offset=526;
           INJ_DutyCycle=900;
      }
    
    BDCFuelPumpEnable();
    
}

      //StartupDC=DC_constant;
    
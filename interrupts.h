/* 
 * File:   interrupts.h
 * Author: dyulov
 *
 * Created on July 25, 2013, 11:07 AM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C"{
#endif

#define PRIORITY_LOW 0
#define PRIORITY_HIGH 1

#ifdef	__cplusplus
}
#endif

#endif	/* INTERRUPTS_H */


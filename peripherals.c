#define USE_OR_MASKS
#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
// Peripheral library headers
#include <timers.h>
#include <compare.h>
#include <capture.h>
#include <spi.h>

#include "scheduler.h"
#include "control.h"
#include "system.h"
#include "interrupts.h"
#include "injector.h"
#include "battery.h"
#include "dac.h"
#include "peripherals.h"
#include "etemp.h"
#include "fixed_point.h"
#include "CalcFuelTypeParameter.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/
//uint16_t DC_out_1[]={      0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0   };
//uint16_t DC_out_2[]={        0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0 ,     0   };
//int a;
//int b;
//int c;
//
//extern uint16_t *ptr1=0;
//extern uint16_t *ptr2=0;





void ADC_Init(void);
void DIO_Init(void);
void PWM_HS_Init(void);
void PWM_HS_Update(uint8_t dutyCycle);
void CaptureInit(void);
void LED_Flash(void);
void SPI_Init (void);

void InitApp(void){
//           for (a=0;a<224; a++){
//      b=a+202;
//      c=a+425;
//
//
//
//      DC_out_1[a]=(uint16_t)(b*b*b*.00000778);//-b*b*.008923+b*3.492-334.92);
//    //  DC_out_1[a]=(int)(DC_out_1[a]+b*3.492-(b*b*.008923));
//      DC_out_2[a]=(uint16_t)(c*c*c*.00000778);//-c*c*.008923+c*3.492-334.92;
//
////    DC_out_1[a]=b*.3;
////    DC_out_2[a]=c*.5;
//    }
//     ptr1=&DC_out_1[0];
//     ptr2=&DC_out_2[0];






    DIO_Init();
    ADC_Init();
    SPI_Init();
    // RS232_Init();
    GenControlInit(); // State machine
    CaptureInit(); // Capture engine speed pulse and calculate period
    SCH_Init(); // Initialize scheduler
    SCH_AddTask(GenControl, 0, 10);
    BatteryMonitorInit(); 
    SCH_AddTask(BatteryMonitor, 5, 500);
    SCH_AddTask(EngTempControl,20,150);
    SCH_AddTask(DAC_Write, 12, 500);
    // SCH_AddTask(LSD_SPI_Write, 7, 500);
    // TODO: Increment SCH_MAX_TASKS
   
 
    
    SCH_Start();


}

// TODO: Function for scheduler testing
void LED_Flash(void){
    LATBbits.LATB1 = ~LATBbits.LATB1; // LED_READY
}

void DIO_Init(void){
    // Port B
    ANSELB = 0; // Set all pins on Port B to digital
    TRISB = 0x00; // Set all pins on Port B to outputs
    // Port C
    ANSELC = 0; // Set all pins on Port C to digital
    // Configure RC2/CCP1 as input for speed sensor input (1) - used with CCP1 and TMR3 to calculate period
    // Configure RC4/SDI1 as input for SPI MISO
    TRISC = 0b00010100;
    
    // Port D pins as digital outputs
    ANSELD = 0; // Set all pins on Port D to digital
    TRISD = 0x00; // Set all pins on Port D to outputs

    // Initialize all DIO to OFF state
    CAG_DISABLE;
    FUEL_PUMP_DISABLE;
    CHRG_DISCONNECT;
    LSD_SPARE_DISABLE;
    LSD_INJECTOR_OFF;    
    OIL_PUMP_DISABLE;
    LSD_DISABLE;
    PWR_HOLD_DISABLE;
    LED_WAIT_OFF;
    LED_READY_OFF;
    LED_FAULT_OFF;
}

// Configure Enhanced CCP1 PWM output on RD7/P1D pin
// TODO: Change to CCP4 if used
void PWM_HS_Init(void){
    
    // Set up 8-bit Timer2 to generate the PWM period (frequency)
    T2CON = 0b00000111;// Prescale = 1:16, timer on, postscale not used with CCP module
    PR2 = 249;         // Timer 2 Period Register = 250 counts
    // PWM frequency is:
    // 64MHz clock / 4 = 16MHz instruction rate.
    // (16MHz / 16 prescale) / 250) = 4KHz, a period of 250us.

    // The Duty Cycle is controlled by the ten-bit CCPR1L<7,0>:DC1B1:DC1B0
    // 50% Duty Cycle = 0.5 * (250 * 4) = 500
    CCPR1L = 0x7D;   // The 8 most significant bits of the value 500 = 0x1F4 are 0x7D
                     // The 2 least significant bits of the value (0b00) are written
                     // to the DC1B1 and DC1B0 bits in CCP1CON
    CCP1CON = 0b01001100;
                     // P1Mx = 01 Full-Bridge output forward, so we get the PWM
                     // signal on P1D to LED7.  Only Single Output (00) is needed,
                     // but the P1A pin does not connect to a demo board LED
                     // CCP1Mx = 1100, PWM mode with P1D active-high.
}

// Input 8-bit value 0-255 to set duty cycle 0-100%
void PWM_HS_Update(uint8_t dutyCycle){
    CCPR1L = dutyCycle; // + 8 including 2 bits DC1Bx in CCP1CON
    PIR1bits.TMR2IF = 0; // clear interrupt flag; set on every TMR2 = PR2 match
    // while (PIR1bits.TMR2IF == 0); // watch for match, which is end of period
}

// 16-bit resolution PWM in compare mode
void PWM_LS_Init(void){
    uint8_t config = 0x0;
    uint16_t pos_pulse_width = 0x0; // +DC = 0%; injector is closed
    //1. Configure CCPx to clear output (CCPx pin) on match in Compare mode
    /* CCP4 "config" parameter settings:
     * ECOM_LO_MATCH - Compare mode: initialize CCPx pin high; on compare match force CCPx pin low
     * COM_INT_OFF - Disable Compare unit interrupt
     * CCP_4_SEL_TMR12 - ECCP selects TIMER1 for Capture & Compare and TIMER2 for PWM
     */
    config = ECOM_LO_MATCH | COM_INT_OFF | ECCP_2_SEL_TMR12;
    // OpenECompare2(config, period); // This function does not work - set registers directly
    CCP4CON = 0x0;
    CCPR4L = pos_pulse_width;        // load CA1L
    CCPR4H = (pos_pulse_width >> 8); // load CA1H
    //2. Enable the Timer1 interrupt and ...
    //3. Set the period of the waveform via Timer1 prescaler
    TMR1_Init(); // Init and start TMR1
    //4. Set the duty cycle of the waveform using CCPR4L and CCPR4H.
    //   pos_pulse_width = (Timer/100) * (+DC%)
    //5. Set CCP4 pin when servicing the Timer1 overflow interrupt
}

void CaptureInit(void){
    uint8_t config=0x00;
    /* 1. Configure control bits CCP1M3:CCPxM0 (CCP1CON<3:0>)
    to capture every falling edge of the waveform. */
    config = CAP_EVERY_FALL_EDGE | CAPTURE_INT_ON | ECCP_1_SEL_TMR34;
    // OpenECapture1(config); // TODO: This function may not work. Set regs directly if needed
    CCP1CON = 0b00000100; // Capture mode: every falling edge
    /* 2. Configure the Timer3 prescaler so Timer3 with run Tmax without overflowing */
    CCPTMRS0bits.C1TSEL = 0x01; // Capture/Compare modes use Timer3
    TMR3_Init();
    /* 3. Enable the CCP interrupt (CCP1IE bit) */
    PIR1bits.CCP1IF = 0;
    PIE1bits.CCP1IE = 1;
    /* 4. When a CCP1 interrupt occurs:
        a) Subtract saved captured time (t1) from captured time (t2) and
        store (use Timer3 interrupt flag as overflow indicator).
        b) Save captured time (t2).
        c) Clear Timer3 flag if set. */
    /* The result obtained in step 4.a is the period (T) */
}

// Initialize the Analog-To-Digital converter.
void ADC_Init(void){
    uint8_t config1=0x00, config2=0x00, config3=0x00;

    CloseADC(); // Disable interrupt and close ADC in case ADC was previously open

    // Select analog or digital input type
    ANSELA = 0;	// Disable all analog inputs on port A
    ANSELE = 0; // Disable analog inputs on port E
    ANSELAbits.ANSA0 = 1; // Set RA0 to analog input AN0 - FUEL_TYPE
    ANSELAbits.ANSA1 = 1; // Set RA1 to analog input AN1 - TPS
    ANSELAbits.ANSA2 = 1; // Set RA2 to analog input AN2 - FUEL_PRESSURE
    ANSELAbits.ANSA3 = 1; // Set RA3 to analog input AN3 - BLOCK_TEMP
    ANSELAbits.ANSA5 = 1; // Set RA5 to analog input AN4 - VSENSE_CHRG
    ANSELEbits.ANSE0 = 1; // Set RE0 to analog input AN5 - VSENSE_BAT
    ANSELEbits.ANSE1 = 1; // Set RE1 to analog input AN6 - ISENSE_CHRG
    ANSELEbits.ANSE2 = 1; // Set RE2 to analog input AN7 - SPARE_AI
    // Select tri-state input or output pin function
    TRISAbits.RA0 = 1; // Set RA0 to input
    TRISAbits.RA1 = 1; // Set RA1 to input
    TRISAbits.RA2 = 1; // Set RA2 to input
    TRISAbits.RA3 = 1; // Set RA3 to input
    TRISAbits.RA5 = 1; // Set RA5 to input
    TRISEbits.RE0 = 1; // Set RE0 to input
    TRISEbits.RE1 = 1; // Set RE1 to input
    TRISEbits.RE2 = 1; // Set RE2 to input

    /* ADC configuration:
    * AN1 for sampling
    * ADC interrupt 0
    * ADC reference voltage from VDD & VSS
    */
    // Conversion clock | Result is justified | Aquisition time
    config1 = ADC_FOSC_64 | ADC_RIGHT_JUST | ADC_20_TAD; // ADCON2 register
    // Channel Select AN0 | ADC interrupt off
    config2 = ADC_CH1 | ADC_INT_OFF; // ADCON0 register
    // Special trigger select | AD Vref+ configuration | AD Vref- configuration
    config3 = ADC_TRIG_CCP5 | ADC_REF_VDD_VDD | ADC_REF_VDD_VSS; // ADCON1 register
    OpenADC(config1, config2, config3);
    
    // Sets bits VCFG1 and VCFG0 in ADCON1 so the ADC voltage reference is VSS to VDD
    // Special Trigger = 0 | Vref+ = 0 | Vref- = 0
    /// ADCON1 = 0;
    // The ADC clock must be as short as possible but still greater than the
    // minimum TAD time, datasheet parameter 130. TAD minimum for the PIC18F45K20 is 1.4us.
    // At 1MHz clock, selecting ADCS = FOSC/2 = 500kHz.  One clock period
    // 1 / 500kHz = 2us, which greater than minimum required 1.4us.
    // So ADCON2 bits ADCS2-0 = 000
    //
    // The ACQT aquisition time should take into accound the internal aquisition
    // time TACQ of the ADC, datasheet paramter 130, and the settling time of
    // of the application circuit connected to the ADC pin.  Since the actual
    // settling time of the RC circuit with the demo board potentiometer is very
    // long but accuracy is not very important to this demo, we'll set ACQT2-0 to
    // 20TAD = 111
    //
    // ADFM = 0 so we can easily read the 8 Most Significant bits from the ADRESH - Left Justified
    // Special Function Register
    // ADCON2 = 0b00111000;
    // Select channel 0 (AN0) to read the potentiometer voltage and turn on ADC
    /// ADCON0 = 0b00000001;
}

// Return 10-bit ADC count on specified channel
// Important: chan must conform to lib format i.e. ADC_CH0, ADC_CH1, ADC_CHn
uint16_t ADC_Convert(uint8_t chan){
    uint16_t timeout = 0xFA00; // 65536-64000 = 1536 (~96 uS)
    SelChanConvADC(chan); // start conversion alt: ADCON0bits.GO_DONE = 1;
    // TODO: Add timeout to while loop
    // && (++timeout != 0)
    while(BusyADC()); // wait for it to complete. alt: while(ADCON0bits.GO_DONE == 1);
    return(ReadADC());
}

// Used to drive scheduler with 1ms interval
void TMR0_Init(void){
    uint8_t config1=0x00;

    WriteTimer0(TMR0_VALUE); // reset timer. alt: TMR0H=0;TMR0L=0;
    INTCON2bits.TMR0IP = PRIORITY_LOW; // Low priority interrupt
    // Timer Width | Clock source | Prescaler | Timer Interrupt enable
    config1 = T0_16BIT | T0_SOURCE_INT | T0_PS_1_1 | TIMER_INT_ON;
    OpenTimer0(config1);

    // Timer0 Interrupt-related registers
    ///INTCONbits.TMR0IF = 0; // clear roll-over interrupt flag
    ///INTCON2bits.TMR0IP = 0; // Low priority interrupt
    ///INTCONbits.TMR0IE = 1; // Enable Timer0 interrupt to CPU
}

// Used for 16-bit PWM in compare mode. Timer value sets PWM Period
// FOSC = 64MHz, Prescaler = 1:4, Timer width = 16-bit
// Frequency = FOSC/4/prescaler/65536 = 64000000/4/4/65536 = 61Hz
// Period = 1/61Hz = 16.4 mS
void TMR1_Init(void){
    uint8_t config=0x00;
    uint8_t config1 = 0x0;

    WriteTimer1(TMR1_VALUE); // Clear timer. alt: TMR1H=0;TMR1L=0;
    /* Timer1 "config" parameter settings:
     * T1_16BIT_RW - Enables register read/write of Timer1 in one 16-bit operation
     * T1_SOURCE_FOSC_4 - Clock source is instruction clock (FOSC/4)
     * T1_PS_1_4 - 1:4 prescale value
     * TIMER_INT_ON - Enable TIMER Interrupt
     * T1_OSC1EN_OFF - Timer 1 oscilator enable off
     * T1_SYNC_EXT_OFF - Do not synchronize external clock input
     */
    // Warning: NOR masks might work, might not - replace w/and
    config = T1_16BIT_RW | T1_SOURCE_FOSC_4 | T1_PS_1_4 | TIMER_INT_ON;
    /* Timer1 "config1" parameter settings:
     * TIMER_GATE_OFF - Timer1 is always counting
     */
    config1 = TIMER_GATE_OFF;
    OpenTimer1(config, config1);
}

// Used for ignition pulse (engine speed) period measurement
// Timer increments at 32.768 KHz and overflows at 0.5 Hz (2 S period)
void TMR3_Init(void){
    uint8_t config=0x00;
    uint8_t config1 = 0x0;

    WriteTimer3(0x0); // Clear timer. alt: TMR1H=0;TMR1L=0;
    /* Timer3 "config" parameter settings:
     * T3_16BIT_RW - Enables register read/write of Timer3 in one 16-bit operation
     * T3_SOURCE_PINOSC - Clock source is external oscillator on SOSCI, SOSCO pins
     * T3_PS_1_1 - 1:1 prescale value
     * TIMER_INT_OFF - Disable TIMER Interrupt
     * T3_OSC1EN_ON - Timer 3 oscilator 32 KHz crystal
     * T3_SYNC_EXT_OFF - Do not synchronize external clock input
     */
    // Warning: NOR masks might work, might not - replace w/and
    config = T3_16BIT_RW | T3_SOURCE_PINOSC | T3_PS_1_1 | TIMER_INT_OFF | T3_OSC1EN_ON;
    /* Timer3 "config1" parameter settings:
     * TIMER_GATE_OFF - Timer3 is always counting
     */
    config1 = TIMER_GATE_OFF;
    OpenTimer3(config, config1);
}

/* Configure and enable interrupts */
void InterruptInit(void){
    INTCONbits.PEIE = 1; // Enable peripheral interrupts
    INTCONbits.GIE = 1; // Enable global Interrrupts
    RCONbits.IPEN = 1; // Enable Interrupt priority feature
    INTCONbits.GIEL = 1; // Enable Low priority interrupts
    INTCONbits.GIEH = 1; // Enable High and Low priority interrupts
}

/* TODO: Move all SPI functions to SPI.c
 Initialize SSP1 module to transmit and receive in master mode */
void SPI_Init (void){
    uint8_t sync_mode=0;
    uint8_t bus_mode=0;
    uint8_t smp_phase=0;

    // Slave select outputs
    SPI_SS_DAC_OFF;
    SPI_SS_LSD_OFF;

    CloseSPI();
    sync_mode = SPI_FOSC_64;
    bus_mode = MODE_00; // Mode is set for DAC SPI (00 or 11)
    smp_phase = SMPMID;
    OpenSPI1(sync_mode, bus_mode, smp_phase);
}

uint8_t SPI_Write16(uint16_t data, uint8_t chan){
    switch(chan){
        case SPI1:
            if(WriteSPI1(data >> 8))return SPI_COMM_TX_BUSY; // msb
            if(WriteSPI1(data & 0xFF))return SPI_COMM_TX_BUSY;// lsb
            break;
        case SPI2:
            if(WriteSPI2(data >> 8))return SPI_COMM_TX_BUSY; // msb
            if(WriteSPI2(data & 0xFF))return SPI_COMM_TX_BUSY;// lsb
            break;
        default:
            break;
    }
    return SPI_COMM_READY;
}

uint16_t SPI_Read16(uint8_t chan){
    uint8_t data_byte = 0;
    uint16_t data = 0;

    data_byte = ReadSPI1();
    data = data_byte << 8;
    data_byte = ReadSPI1();
    data += data_byte;
    return data;
}

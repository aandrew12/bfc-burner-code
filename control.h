/* 
 * File:   states.h
 * Author: dyulov
 *
 * Created on July 25, 2013, 3:07 PM
 */

#ifndef CONTROL_H
#define	CONTROL_H

#ifdef	__cplusplus
extern "C"{
#endif

#define TMR_DRAIN_TIMEOUT 30000U // 300s / 0.01s (ticks)
#define TMR_WAIT_TIMEOUT 1500U // 15s / 0.01s (ticks) TODO: Should be temp compensated
#define TMR_START_TIMEOUT 1000U // 10s / 0.01s (ticks)
#define TMR_IGN_PULSE_TIMEOUT 25U //  0.25s / 0.01s (ticks)
// Igniton Pulse period is calculated using 32768 Hz Timer3
#define IGN_PULSE_PERIOD_RUN 640U // Assume that engine is running at this point (640U = 3072 RPM, 1000U = 1966 RPM)
 //   #define IGN_PULSE_PERIOD_RUN 1200U // Assume that engine is running at this point (640U = 3072 RPM, 1000U = 1966 RPM)
//#define IGN_PULSE_PERIOD_MIN 128U //15360 RPM, 256U = 7680 RPM
#define IGN_PULSE_PERIOD_MIN 20U //15360 RPM, 256U = 7680 RPM
#define IGN_PULSE_PERIOD_MAX 4096U // 480 RPM

typedef enum{
    GEN_STATE_INIT =    0x00,
    GEN_STATE_DRAIN =   0x01,
    GEN_STATE_WAIT =    0x02,
    GEN_STATE_START =   0x03,
    GEN_STATE_RUN =     0x04,
    GEN_STATE_CAL =     0x05,
    GEN_STATE_OFF =     0xFF
}GenCtrlState_t;

typedef enum{
    FUEL_TYPE_FLT = 0x00,
    FUEL_TYPE_GAS = 0x01,
    FUEL_TYPE_JP8 = 0x02,
    FUEL_TYPE_DSL = 0x03
}FuelType_t;

void GenControlInit(void);
void GenControl(void);
void GetIgnitionPeriod(void);

#ifdef	__cplusplus
}
#endif

#endif	/* CONTROL_H */


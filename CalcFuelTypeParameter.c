/*
 * File:   CalcFuelTypeParameter.c
 * Author: labrat
 *
 * Created on March 28, 2016, 11:30 AM
 */



#include <xc.h>
// Standard library headers
#include <stdint.h>
#include <stdbool.h>


#include <stdlib.h>
#include "fixed_point.h"
#include "peripherals.h"

#include "control.h"
#include "config.h"

#include "types.h"
#include "scheduler.h"


#include "types.h"
#include "peripherals.h"
#include "CalcFuelTypeParameter.h"



extern uint16_t TempSet=278;
extern uint16_t TempThrStep12=82;
extern uint16_t TempThrStep11=80;
extern uint16_t TempThrStep9=70;
extern uint16_t TempThrStep7=60;
int i=0;


extern double time_mult_cold[]={2.0, 2.6,2.6, 3.0, 2.75, 2.0, 1.9, 1.8, 1.6, 1.5, 1.25,1.0};
extern double time_mult_warm[]={2.5, 2.5,2.5, 2.5, 2.5, 2.25, 1.8, 1.6, 1.4, 1.25, 1.1,1.0};

extern int cag_offset_cold[]={575, 575, 575, 500, 500, 491, 491, 491, 491, 461, 414, 400};
extern int cag_offset_warm[]={575, 570, 550, 500, 490, 491, 481, 481, 461, 461, 414, 400};


extern FuelType_t FuelType;



void CagOffsetInit(void){
     if (FuelType==FUEL_TYPE_DSL){}
     else if (FuelType==FUEL_TYPE_GAS){}
}

void TimeMultInit(void){
    if  (FuelType==FUEL_TYPE_DSL){
        for(i=0;i<12;i++){
            time_mult_cold[i]=time_mult_cold[i];//*1.08;
            time_mult_warm[i]=time_mult_warm[i]*.9;
        }
    }

      if  (FuelType==FUEL_TYPE_GAS){

          for(i=3;i<12;i++){
            time_mult_cold[i]=1;
            time_mult_warm[i]=1;
        }
    }


}

void TempParametersInit(void){






    if  (FuelType==FUEL_TYPE_GAS){
        TempSet=260;
        TempThrStep12=70;
        TempThrStep11=60;
        TempThrStep9=52;
        TempThrStep7=45;

    }

    else if  (FuelType==FUEL_TYPE_DSL){
        TempSet=310;
        TempThrStep12=93; //93
        TempThrStep11=86;
        TempThrStep9=80;
        TempThrStep7=75;
    }
        else if  (FuelType==FUEL_TYPE_JP8){
        TempSet=278; //86
        TempThrStep12=82;
        TempThrStep11=80;
        TempThrStep9=70;
        TempThrStep7=60;

    }

    

    }


